<?php //include('server.php') ?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>User Registration</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<link rel="stylesheet" href="../../assets/css/styleRegister.css">
</head>
<!-- navbar -->

<body>
<!-- the box with the login and registration -->
<div class="login-box">
    <div class="lb-header">
      <a href="#" class="active" id="login-box-link">Login</a>
      <a href="#" id="signup-box-link">Sign Up</a>
    </div>
    <header style = "color:gray; font-size:25px"> Sign in to your account </header>

    <!-- social media login -->
    <div class="social-login">
      <a href="#">
        <i class="fa fa-facebook fa-lg"></i>
        Login in with Facebook
      </a>
      <a href="#">
        <i class="fa fa-google-plus fa-lg"></i>
        Login with Google
      </a>
    </div>

    <!-- login through email-->

    <form class="email-login" method = "post" action="../home/defaultIndividual.php">
      <div class="form-group">
        <!-- <input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $email; ?>"> -->
        <input type="email" class="form-control" placeholder="Email" name="email" value="">
      </div>
      <div class="form-group">
        <input type="password" class="form-control" placeholder="Password" name="password"/>
      </div>
      <!-- Sign up confirmation with user -->
      <header style = "color:grey; font-size:15px; text-align: center;"> By clicking Login, you agree to our <a href="empty.php">Terms of Service</a> and <a href="empty.php">Privacy Policy</a>. </header>
  <br>

      <!-- Log in -->

      <div class="u-form-group">
        <button type = "submit" class="btn" name="login_user">Log in</button>
      </div>

      <!-- Forgot Password -->

      <div class="form-group">
        <a href="forgotPassword.php" class="forgot-password" style="font-size:15px;">Forgot password?</a>
      </div>
    </form>

    <!-- sign up page -->

    <form class="email-signup" method = "post" action="registerUser.php">

    <div class="row">
   <div class="form-group col-xs-6">
       <input id="firstname" class="form-control input-group-lg reg_name" type="text" name="firstname" title="Enter first name" placeholder="First name"/>
       </div> <div class="form-group col-xs-6"> 
          <input id="lastname" class="form-control input-group-lg reg_name" type="text" name="lastname" title="Enter last name" placeholder="Last name"/>
         </div>
         </div>
      <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" name="email" >
      </div>
    <div class="row">
   <div class="form-group col-xs-6">
       <input id="password_1" class="form-control input-group-lg reg_name" type="password" name="password_1" title="Enter password" placeholder="Enter password"
       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
       </div> <div class="form-group col-xs-6"> 
          <input id="password_2" class="form-control input-group-lg reg_name" type="password" name="password_2" title="Confirm password" placeholder="Confirm password"/>
         </div>
         </div>
         <!-- Sign up confirmation with user -->
         <header style = "color:grey; font-size:15px; text-align: center;"> By clicking Sign Up, you agree to our <a href="empty.php">Terms of Service</a> and <a href="empty.php">Privacy Policy</a>. </header>
    <br>

    <!-- Submit button - connects with server. Now with validation to set if password_1 and password_2 are equal or not
     - message will be displayed real-time. Can be edited from the signScript.js on line 81. -->
    <span id="confirm_password_msg"></span>
      <div class="u-form-group">
        <button type ="submit" class="btn" name="reg_user">Sign Up</button>
      </div>
    </form>
  </div>

  <div id="message">
  <h3>Password must contain the following:</h3>
  <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
  <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
  <p id="number" class="invalid">A <b>number</b></p>
  <p id="length" class="invalid">Minimum <b>8 characters</b></p>
</div>

<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script  src="../../assets/js/signScript.js"></script>

</body>
</html>
