<?php
session_start();

// connects to the database
$host        = "host = 127.0.0.1";
$port        = "port = 5432";
$dbname      = "dbname = trustPeople_db";
$credentials = "user = postgres password=Zerothemegaman5!";

$conn = pg_connect( "$host $port $dbname $credentials"  );
if(!$conn) {
   echo "Error : Unable to open database<br>";
} else {
   // echo "Opened database successfully<br>";
}

// initialization 
$email    = "";
$errors = array(); 

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receives input value
  $email = $_POST['email'];
  $firstname = $_POST['firstname'];
  $lastname = $_POST['lastname'];
  $password_1 = $_POST['password_1'];
  $password_2 = $_POST['password_2'];

  // form validation

  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if (empty($firstname)) { array_push($errors, "Please enter your first name"); }
  if (empty($lastname)) { array_push($errors, "Please enter your last name"); }
  if ($password_1 != $password_2) {
	array_push($errors, "The two passwords do not match");
  }
// validates email - gives actual "instruction" on what is needed before submitting the registration, unlike the top validation
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format";
    
  }

 
  //checks for existing users - if with same from database, then it creates error for "exist"
  $user_check_query = "SELECT * FROM registration WHERE email='$email' LIMIT 1";
  $result = pg_query($conn, '$user_check_query');
  $user = pg_fetch_assoc($result);
  
  if ($user) { // if user exists or not - checks with database
    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  }

  // after check registration for no errors, uses this function to insert.
  if (count($errors) == 0) {
    $password = ($password_1); //this encrypts password before saving to database
    
  	$query = "INSERT INTO registration (email, password, firstname, lastname) VALUES('$email', '$password', '$firstname', '$lastname')";
pg_query($conn, $query);
  	$_SESSION['email'] = $email;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: index.php');
  }
}

// error checker and confirmation in the login system.
if (isset($_POST['login_user'])) {
    $email = pg_escape_string($conn, $_POST['email']);
    $password = pg_escape_string($conn, $_POST['password']);
  
    if (empty($email)) {
        array_push($errors, "Email is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }
  
    if (count($errors) == 0) {
        $password = ($password);
        $query = "SELECT * FROM registration WHERE email='$email' AND password='$password'";
        $results = pg_query($conn, $query);
        if (pg_num_rows($results) == 1) {
          $_SESSION['email'] = $email;
          $_SESSION['success'] = "You are now logged in";
          header('location: index.php');
        }else {
            array_push($errors, "Wrong email/password combination");
        }
    }
  }
  ?>