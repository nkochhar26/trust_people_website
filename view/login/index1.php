<!DOCTYPE html>
<html>
<head>
	<title>CRUD page</title>

	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>
<body>
	<style type="text/css">
		#overlay{
			position: fixed;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			background: rgba(0,0,0,0.6);
		}

	</style>
	<div id="app">
		<div class="container-fluid">
			<div class="row bg-dark">
				<div class="col-lg-12">
					<p class="text-center text-light display-4 pt-2" style="font-size: 25px;">CRUD page</p>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row mt-3">
				<div class="col-lg-6">
					<h3 class="text-info">Enter User Details</h3>
				</div>
				<div class="col-lg-6">
					<button class="btn btn-info float-right" @click="showAddModal = true">
						<i class="fa fa-user"> Add New Users</i>
					</button>
				</div>
			</div>
			<hr class="bg-info">
			<div class="alert alert-danger" v-if="errorMsg">
				{{errorMsg}}
			</div>
			<div class="alert alert-success" v-if="successMsg">
				{{successMsg}}
			</div>

			<div class="row">
				<div class="col-lg-12">
					<table class="table table-bordered table-striped">
						<thead class="text-center bg-info text-light">
							<th>ID</th>
							<th>Email</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Password</th>
							<th>Add or remove users</th>
						</thead>
						<tbody>
							<tr class="text-center" v-for="user in users">
								<td>{{ user.id }}</td>
								<td>{{ user.email}}</td>
								<td>{{ user.firstname}}</td>
								<td>{{ user.lastname}}</td>
								<td>{{ user.password}}</td>
								<td>
									<a href="#" class="text-success" @click="showEditModal = true; selectUser(user);"><i class="fa fa-edit"></i></a>
									|
									<a href="#" class="text-danger" @click="showDeleteModal = true; selectUser(user);"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!-- show add modal popup -->
		<div id="overlay" v-if="showAddModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Add New User</h5>
						<button type="button" class="close" @click="showAddModal = false">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body p-4">
						<form action="#" method="POST">
							<div class="form-group">
								<input type="id" name="id" class="form-control form-control-lg" placeholder="Enter ID" v-model="newUser.id">
							</div>
							<div class="form-group">
								<input type="text" name="email" class="form-control form-control-lg" placeholder="Enter Email" v-model="newUser.email">
							</div>
							<div class="form-group">
								<input type="text" name="firstname" class="form-control form-control-lg" placeholder="Enter First Name" v-model="newUser.firstname">
							</div>
							<div class="form-group">
								<input type="text" name="lastname" class="form-control form-control-lg" placeholder="Enter Last Name" v-model="newUser.lastname">
							</div>
							<div class="form-group">
								<input type="text" name="password" class="form-control form-control-lg" placeholder="Enter Password" v-model="newUser.password">
							</div>
							<div class="form-group">
								<button class="btn btn-success btn-block btn-lg" @click="showAddModal = false; addUser(); clearMsg();">Add User</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- edit add modal popup -->
		<div id="overlay" v-if="showEditModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Edit User</h5>
						<button type="button" class="close" @click="showEditModal = false">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body p-4">
						<form action="#" method="POST">
							<input type="hidden" name="id" v-model="currentUser.id">
							<div class="form-group">
								<input type="text" name="email" class="form-control form-control-lg" v-model="currentUser.email">
							</div>
							<div class="form-group">
								<input type="text" name="firstname" class="form-control form-control-lg" v-model="currentUser.firstname">
							</div>
							<div class="form-group">
								<input type="text" name="lastname" class="form-control form-control-lg" v-model="currentUser.lastname">
							</div>
							<div class="form-group">
								<input type="text" name="password" class="form-control form-control-lg" v-model="currentUser.password">
							</div>
							<div class="form-group">
								<button class="btn btn-success btn-block btn-lg" @click="showEditModal = false; updateUser(); clearMsg();">Update User</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- delete add modal popup -->
		<div id="overlay" v-if="showDeleteModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Delete User</h5>
						<button type="button" class="close" @click="showDeleteModal = false">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body p-4">
						<h4 class="text-danger">You're about to delete a user.</h4>
						<h5>You are deleting '{{ currentUser.name }}'</h5>
						<hr>
						<button class="btn btn-danger btn-lg" @click="showDeleteModal = false; deleteUser(); clearMsg();"> Yes</button>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<button class="btn btn-default btn-lg" @click="showDeleteModal = false"> No</button>
					</div>
				</div>
			</div>
		</div>
	</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			errorMsg : "",
			successMsg : "",
			showAddModal : false,
			showEditModal : false,
			showDeleteModal : false,
			users : [],
			newUser : {id: "", email: "", firstname: "", lastname:"", password: ""},
			currentUser : {},
			// a : 1
		},
		mounted: function(){
			this.getAllUser();
			// this.a+1;
		},
		methods: {
			getAllUser(){
				axios.get("http://localhost/registration/model.php?action=read").then(function(response){
					if(response.data.error){
						app.errorMsg = response.data.message;
					}
					else{
						app.users = response.data.users;
					}
				});
			},
			addUser(){
				var formData = app.toFormData(app.newUser);
				axios.post("http://localhost/registration/model.php?action=create", formData).then(function(response){
					app.newUser = {id: "", email: "", firstname: "", lastname: "", password: ""};
					if(response.data.error){
						app.errorMsg = response.data.message;
					}
					else{
						app.successMsg = response.data.message;
						app.getAllUser();
					}
				});
			},
			updateUser(){
				var formData = app.toFormData(app.currentUser);
				axios.post("http://localhost/registration/model.php?action=update", formData).then(function(response){
					app.currentUser = {};
					if(response.data.error){
						app.errorMsg = response.data.message;
					}
					else{
						app.successMsg = response.data.message;
						app.getAllUser();
					}
				});
			},
			deleteUser(){
				var formData = app.toFormData(app.currentUser);
				axios.post("http://localhost/registration/model.php?action=delete", formData).then(function(response){
					app.currentUser = {};
					if(response.data.error){
						app.errorMsg = response.data.message;
					}
					else{
						app.successMsg = response.data.message;
						app.getAllUser();
					}
				});
			},
			toFormData(obj){
				var fd = new FormData();
				for(var i in obj){
					fd.append(i, obj[i]);
				}
				return fd;
			},
			selectUser(user){
				app.currentUser = user;
			},
			clearMsg(){
				app.errorMsg = "";
				app.successMsg = "";
			}
		}
	});
</script>
</body>
</html>