<?php //include('server.php') ?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Enterprise Registration</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<link rel="stylesheet" href="../../assets/css/styleRegister.css">
</head>
<!-- navbar -->


<body>
<!-- the box with the login and registration -->
<div class="login-box">
    <div class="lb-header">
    </div>
    <header style = "color:lightskyblue; font-size:30px; text-align: center;"> Create your Enterprise account </header>
    <header style = "color:grey; font-size:20px; text-align: center;"> Sign up for Trustpeople Enterprise account </header>

    <!-- register  -->
    <form class="email-signup" method = "post" action="../home/defaultEnterprise.php">

    <div class="row">
   <div class="form-group col-xs-6">
       <input id="firstname" class="form-control input-group-lg reg_name" type="text" name="firstname" title="Enter first name" placeholder="First name"/>
       </div> <div class="form-group col-xs-6"> 
          <input id="lastname" class="form-control input-group-lg reg_name" type="text" name="lastname" title="Enter last name" placeholder="Last name"/>
         </div>
         </div>
    <div class="row">
   <div class="form-group col-xs-6">
       <input id="title" class="form-control input-group-lg reg_name" type="text" name="title" title="Title" placeholder="Title"/>
       </div> <div class="form-group col-xs-6"> 
          <input id="companyemail" class="form-control input-group-lg reg_name" type="email" name="companyemail" title="Company Email" placeholder="Company Email"/>
         </div>
         </div>
         <div class="row">
   <div class="form-group col-xs-6">
       <input id="phonenumber" class="form-control input-group-lg reg_name" type="text" name="phonenumber" title="Phone Number" placeholder="Phone Number"/>
       </div> <div class="form-group col-xs-6"> 
          <input id="companyname" class="form-control input-group-lg reg_name" type="url" name="companyname" title="Company Name" placeholder="Company Name"/>
         </div>
         </div>
         <div class="form-group">
        <input type="text" class="form-control" placeholder="Company Website" name="companywebsite" pattern="https?://.+" />
      </div>
      <header style = "color:grey; font-size:15px; text-align: center;"> By clicking Sign Up, you agree to our <a href="empty.php">Terms of Service</a> and <a href="empty.php">Privacy Policy</a>. </header>
      <br>
      <div class="u-form-group">
        <button type ="submit" class="btn" name="reg_enterprise">Sign Up</button>
      </div>
    </form>
  </div>
<!-- partial -->


</body>
</html>
